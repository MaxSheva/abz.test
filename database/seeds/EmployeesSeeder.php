<?php

use Illuminate\Database\Seeder;

class EmployeesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $hr = [];                                                                                   // массив с коллекциями сотрудников одного уровня иерархии(сотрудники сгруппированы по руководителю)
        $employee_collection = [];                                                                  // коллекция сотрудников, сгруппированных по руководителю
        $temporary_hr = [];                                                                         // временный массив для одной итерации цикла создания уровня иерархии сотрудников (руководитель-подчинённые)
        $hierarchy_level = 3;                                                                       // требуемый уровень иерархии
        $root_node_quantity = 2;                                                                    // количество корневых узлов

        for ($i = 0; $i < $hierarchy_level; $i++) {                                                 // количество уровней иерархии
            if ($i == 0) {
                $hr[0] = factory(App\Employee::class, $root_node_quantity)->create(['bossId' => $i]);                 // массив с коллекциями сотрудников
            }

            $collection_quantity_in_hr_array = count($hr);                                          // колличество коллекций сотрудников в массиве hr

            for ($e = 0; $e < $collection_quantity_in_hr_array; $e++) {
                $employee_quantity_in_collection = count($hr[$e]);                                  // колличество сотрудников в коллекции
                for ($j = 0; $j < $employee_quantity_in_collection; $j++) {
                    $employee_collection = factory(App\Employee::class, 2)->create(['bossId' => $hr[$e][$j]->id]); // id j-того элемента из коллекции

                    array_push($temporary_hr, $employee_collection);
                }
            }

            $hr = $temporary_hr;     // перезаписываем массив (новый уровень иерархии руководитель->подчинённые)
            $temporary_hr = [];      // очищаем временный массив, для записи в него новых данных следующего уровня иерархии
        }
    }
}
