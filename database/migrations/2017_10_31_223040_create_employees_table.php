<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bossId')->unsigned()->nullabel()->comment('id руководителя');
            $table->string('photo')->required()->comment('фото сотрудника');
            $table->string('name')->required()->comment('ФИО');
            $table->string('position')->required()->comment('должность');
            $table->date('hire_date')->required()->comment('дата приёма на работу');
            $table->integer('salary')->unsigned()->default(0)->comment('зарплата');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
