<?php

use Faker\Generator as Faker;

/* @var Illuminate\Database\Eloquent\Factory $factory */

$factory->define(App\Employee::class, function (Faker $faker) {
    return [
        'bossId' => 1,
        'photo' => 'images/uploads/employees/default.jpg',
        'name' => $faker->firstName . ' ' . $faker->lastName . ' ' . $faker->firstNameFemale,
        'position' => $faker->jobTitle,
        'hire_date' => $faker->date('Y-m-d', 'now'),
        'salary' => $faker->numberBetween(3200, 90000)
    ];
});
